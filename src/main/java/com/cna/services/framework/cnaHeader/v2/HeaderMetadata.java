package com.cna.services.framework.cnaHeader.v2;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for HeaderMetadata:
{
  "required": [
    "Id",
    "Timestamp",
    "metadataContractVersion"
  ],
  "type": "object",
  "properties": {
    "metadataContractVersion": {
      "$ref": "metadataContractVersion"
    },
    "Id": {
      "$ref": "Id"
    },
    "Timestamp": {
      "$ref": "Timestamp"
    }
  }
}
*/

public class HeaderMetadata {

	@Size(max=1)
	private String metadataContractVersion;

	@Size(max=1)
	private String id;

	@Size(max=1)
	private String timestamp;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    metadataContractVersion = new com.cna.services.framework.cnaHeader.v2.null();
	    id = new com.cna.services.framework.cnaHeader.v2.null();
	    timestamp = new com.cna.services.framework.cnaHeader.v2.null();
	}
	public String getMetadataContractVersion() {
		return metadataContractVersion;
	}
	
	public void setMetadataContractVersion(String metadataContractVersion) {
		this.metadataContractVersion = metadataContractVersion;
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
}