package com.cna.services.framework.cnaHeader.v2;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for To:
{
  "required": [
    "QOS",
    "operationName",
    "serviceName",
    "version"
  ],
  "type": "object",
  "properties": {
    "version": {
      "$ref": "version"
    },
    "serviceName": {
      "$ref": "serviceName"
    },
    "operationName": {
      "$ref": "operationName"
    },
    "QOS": {
      "$ref": "QOS"
    }
  }
}
*/

public class To {

	@Size(max=1)
	private String version;

	@Size(max=1)
	private String serviceName;

	@Size(max=1)
	private String operationName;

	@Size(max=1)
	private String qOS;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    version = new com.cna.services.framework.cnaHeader.v2.null();
	    serviceName = new com.cna.services.framework.cnaHeader.v2.null();
	    operationName = new com.cna.services.framework.cnaHeader.v2.null();
	    qOS = new com.cna.services.framework.cnaHeader.v2.null();
	}
	public String getVersion() {
		return version;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}
	public String getServiceName() {
		return serviceName;
	}
	
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getOperationName() {
		return operationName;
	}
	
	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}
	public String getQOS() {
		return qOS;
	}
	
	public void setQOS(String qOS) {
		this.qOS = qOS;
	}
}