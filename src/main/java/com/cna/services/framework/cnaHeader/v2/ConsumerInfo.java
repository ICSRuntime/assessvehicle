package com.cna.services.framework.cnaHeader.v2;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for ConsumerInfo:
{
  "required": [
    "cnaApplicationName",
    "cnaCheckPermission",
    "cnaConsumerPlatform",
    "cnaLanguage",
    "cnaLocale",
    "cnaLogLevel"
  ],
  "type": "object",
  "properties": {
    "cnaApplicationName": {
      "$ref": "cnaApplicationName"
    },
    "cnaCheckPermission": {
      "$ref": "cnaCheckPermission"
    },
    "cnaConsumerPlatform": {
      "$ref": "cnaConsumerPlatform"
    },
    "cnaLanguage": {
      "$ref": "cnaLanguage"
    },
    "cnaLocale": {
      "$ref": "cnaLocale"
    },
    "cnaLogLevel": {
      "$ref": "cnaLogLevel"
    }
  }
}
*/

public class ConsumerInfo {

	@Size(max=1)
	private String cnaApplicationName;

	@Size(max=1)
	private String cnaCheckPermission;

	@Size(max=1)
	private String cnaConsumerPlatform;

	@Size(max=1)
	private String cnaLanguage;

	@Size(max=1)
	private String cnaLocale;

	@Size(max=1)
	private String cnaLogLevel;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    cnaApplicationName = new com.cna.services.framework.cnaHeader.v2.null();
	    cnaCheckPermission = new com.cna.services.framework.cnaHeader.v2.null();
	    cnaConsumerPlatform = new com.cna.services.framework.cnaHeader.v2.null();
	    cnaLanguage = new com.cna.services.framework.cnaHeader.v2.null();
	    cnaLocale = new com.cna.services.framework.cnaHeader.v2.null();
	    cnaLogLevel = new com.cna.services.framework.cnaHeader.v2.null();
	}
	public String getCnaApplicationName() {
		return cnaApplicationName;
	}
	
	public void setCnaApplicationName(String cnaApplicationName) {
		this.cnaApplicationName = cnaApplicationName;
	}
	public String getCnaCheckPermission() {
		return cnaCheckPermission;
	}
	
	public void setCnaCheckPermission(String cnaCheckPermission) {
		this.cnaCheckPermission = cnaCheckPermission;
	}
	public String getCnaConsumerPlatform() {
		return cnaConsumerPlatform;
	}
	
	public void setCnaConsumerPlatform(String cnaConsumerPlatform) {
		this.cnaConsumerPlatform = cnaConsumerPlatform;
	}
	public String getCnaLanguage() {
		return cnaLanguage;
	}
	
	public void setCnaLanguage(String cnaLanguage) {
		this.cnaLanguage = cnaLanguage;
	}
	public String getCnaLocale() {
		return cnaLocale;
	}
	
	public void setCnaLocale(String cnaLocale) {
		this.cnaLocale = cnaLocale;
	}
	public String getCnaLogLevel() {
		return cnaLogLevel;
	}
	
	public void setCnaLogLevel(String cnaLogLevel) {
		this.cnaLogLevel = cnaLogLevel;
	}
}