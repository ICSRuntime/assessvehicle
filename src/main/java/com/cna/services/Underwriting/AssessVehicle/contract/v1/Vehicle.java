package com.cna.services.Underwriting.AssessVehicle.contract.v1;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Vehicle:
{
  "allOf": [
    {
      "$ref": "Vehicle"
    },
    {
      "properties": {
        "correctedVIN": {
          "type": "string"
        },
        "returnCode": {
          "type": "string"
        },
        "correctedPrimaryClassCode": {
          "type": "string"
        },
        "nonStandardVINOverride": {
          "type": "string"
        },
        "notFoundOverride": {
          "type": "string"
        },
        "availableVINOverride": {
          "type": "string"
        }
      }
    }
  ]
}
*/

public class Vehicle {

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	}
}