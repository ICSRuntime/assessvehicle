package com.cna.services.Underwriting.AssessVehicle.contract.v1;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for RetrieveVehicle:
{
  "type": "object",
  "properties": {
    "serviceTechnicalObject": {
      "$ref": "serviceTechnicalObject"
    },
    "sourceName": {
      "type": "string"
    },
    "vehicle": {
      "type": "array",
      "items": {
        "required": [
          "vin"
        ],
        "type": "object",
        "properties": {
          "vin": {
            "type": "string"
          },
          "makeName": {
            "type": "string"
          },
          "modelYear": {
            "type": "string"
          },
          "garageStateCode": {
            "type": "string"
          }
        }
      }
    }
  }
}
*/

public class RetrieveVehicle {

	@Size(max=1)
	private com.cna.services.Underwriting.AssessVehicle.contract.v1.ServiceTechnicalObject serviceTechnicalObject;

	@Size(max=1)
	private String sourceName;

	@Size(max=1)
	private List<Vehicle> vehicle;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    serviceTechnicalObject = null;
	    sourceName = null;
	    vehicle = new ArrayList<Vehicle>();
	}
	public com.cna.services.Underwriting.AssessVehicle.contract.v1.ServiceTechnicalObject getServiceTechnicalObject() {
		return serviceTechnicalObject;
	}
	
	public void setServiceTechnicalObject(com.cna.services.Underwriting.AssessVehicle.contract.v1.ServiceTechnicalObject serviceTechnicalObject) {
		this.serviceTechnicalObject = serviceTechnicalObject;
	}
	public String getSourceName() {
		return sourceName;
	}
	
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	public List<Vehicle> getVehicle() {
		return vehicle;
	}
	
	public void setVehicle(List<Vehicle> vehicle) {
		this.vehicle = vehicle;
	}
}