package com.cna.services.Underwriting.AssessVehicle.contract.v1;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for RetrieveVehicleResponse:
{
  "required": [
    "businessError",
    "vehicle"
  ],
  "type": "object",
  "properties": {
    "businessError": {
      "$ref": "ErrorInfo"
    },
    "serviceTechnicalObject": {
      "$ref": "serviceTechnicalObject"
    },
    "sourceName": {
      "type": "string"
    },
    "vehicle": {
      "$ref": "vehicle"
    }
  }
}
*/

public class RetrieveVehicleResponse {

	@Size(max=1)
	private com.cna.services.common.commondatatypes.AssessVehicle.v1.ErrorInfo businessError;

	@Size(max=1)
	private ServiceTechnicalObject serviceTechnicalObject;

	@Size(max=1)
	private String sourceName;

	@Size(max=1)
	private com.cna.services.Underwriting.AssessVehicle.contract.v1.Vehicle vehicle;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    businessError = new com.cna.services.Underwriting.AssessVehicle.contract.v1.BusinessError();
	    serviceTechnicalObject = null;
	    sourceName = null;
	    vehicle = new com.cna.services.Underwriting.AssessVehicle.contract.v1.Vehicle();
	}
	public com.cna.services.common.commondatatypes.AssessVehicle.v1.ErrorInfo getBusinessError() {
		return businessError;
	}
	
	public void setBusinessError(com.cna.services.common.commondatatypes.AssessVehicle.v1.ErrorInfo businessError) {
		this.businessError = businessError;
	}
	public ServiceTechnicalObject getServiceTechnicalObject() {
		return serviceTechnicalObject;
	}
	
	public void setServiceTechnicalObject(ServiceTechnicalObject serviceTechnicalObject) {
		this.serviceTechnicalObject = serviceTechnicalObject;
	}
	public String getSourceName() {
		return sourceName;
	}
	
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	public com.cna.services.Underwriting.AssessVehicle.contract.v1.Vehicle getVehicle() {
		return vehicle;
	}
	
	public void setVehicle(com.cna.services.Underwriting.AssessVehicle.contract.v1.Vehicle vehicle) {
		this.vehicle = vehicle;
	}
}