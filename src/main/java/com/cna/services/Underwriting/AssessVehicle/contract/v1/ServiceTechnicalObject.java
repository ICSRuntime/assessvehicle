package com.cna.services.Underwriting.AssessVehicle.contract.v1;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for ServiceTechnicalObject:
{
  "type": "object",
  "properties": {
    "requestType": {
      "type": "string"
    },
    "genericTechnicalReference": {
      "required": [
        "codeName",
        "codeValue"
      ],
      "type": "object",
      "properties": {
        "codeName": {
          "type": "string"
        },
        "codeValue": {
          "type": "string"
        }
      }
    },
    "serviceConsumerKey": {
      "type": "string"
    },
    "serviceProviderKey": {
      "type": "string"
    }
  }
}
*/

public class ServiceTechnicalObject {

	@Size(max=1)
	private String requestType;

	@Size(max=1)
	private com.cna.services.Underwriting.AssessVehicle.contract.v1.GenericTechnicalReference genericTechnicalReference;

	@Size(max=1)
	private String serviceConsumerKey;

	@Size(max=1)
	private String serviceProviderKey;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    requestType = null;
	    genericTechnicalReference = null;
	    serviceConsumerKey = null;
	    serviceProviderKey = null;
	}
	public String getRequestType() {
		return requestType;
	}
	
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public com.cna.services.Underwriting.AssessVehicle.contract.v1.GenericTechnicalReference getGenericTechnicalReference() {
		return genericTechnicalReference;
	}
	
	public void setGenericTechnicalReference(com.cna.services.Underwriting.AssessVehicle.contract.v1.GenericTechnicalReference genericTechnicalReference) {
		this.genericTechnicalReference = genericTechnicalReference;
	}
	public String getServiceConsumerKey() {
		return serviceConsumerKey;
	}
	
	public void setServiceConsumerKey(String serviceConsumerKey) {
		this.serviceConsumerKey = serviceConsumerKey;
	}
	public String getServiceProviderKey() {
		return serviceProviderKey;
	}
	
	public void setServiceProviderKey(String serviceProviderKey) {
		this.serviceProviderKey = serviceProviderKey;
	}
}