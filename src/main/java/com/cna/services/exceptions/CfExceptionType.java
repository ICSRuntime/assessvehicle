package com.cna.services.exceptions;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for CfExceptionType:
{
  "required": [
    "code",
    "currentStackTrace",
    "message",
    "uniqueId"
  ],
  "type": "object",
  "properties": {
    "code": {
      "type": "string"
    },
    "uniqueId": {
      "type": "string"
    },
    "message": {
      "type": "string"
    },
    "currentStackTrace": {
      "type": "string"
    }
  }
}
*/

public class CfExceptionType {

	@Size(max=1)
	@NotNull
	private String code;

	@Size(max=1)
	@NotNull
	private String uniqueId;

	@Size(max=1)
	@NotNull
	private String message;

	@Size(max=1)
	@NotNull
	private String currentStackTrace;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    code = org.apache.commons.lang3.StringUtils.EMPTY;
	    uniqueId = org.apache.commons.lang3.StringUtils.EMPTY;
	    message = org.apache.commons.lang3.StringUtils.EMPTY;
	    currentStackTrace = org.apache.commons.lang3.StringUtils.EMPTY;
	}
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCurrentStackTrace() {
		return currentStackTrace;
	}
	
	public void setCurrentStackTrace(String currentStackTrace) {
		this.currentStackTrace = currentStackTrace;
	}
}