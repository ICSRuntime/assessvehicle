package com.cna.services.Policy.cnaPolicy.PolicyDSM.AssessVehicle.v1;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Price:
{
  "type": "object",
  "properties": {
    "type": {
      "type": "string"
    },
    "amount": {
      "type": "number",
      "format": "double"
    }
  }
}
*/

public class Price {

	@Size(max=1)
	private String type;

	@Size(max=1)
	private Double amount;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    type = null;
	    
	}
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	public Double getAmount() {
		return amount;
	}
	
	public void setAmount(Double amount) {
		this.amount = amount;
	}
}