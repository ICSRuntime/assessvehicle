package com.cna.services.Policy.cnaPolicy.PolicyDSM.AssessVehicle.v1;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Vehicle:
{
  "type": "object",
  "properties": {
    "vehicleNumber": {
      "description": "vehicle sequnce/ occurence number",
      "type": "integer",
      "format": "int32"
    },
    "vin": {
      "type": "string"
    },
    "garageStateCode": {
      "type": "string"
    },
    "engineCylinderCount": {
      "type": "string"
    },
    "typeCode": {
      "description": "code value of the vehicle type. POLK will send a code value ",
      "type": "string"
    },
    "makeCode": {
      "description": "contains the Polk standardized abbreviation for the OEMs vehicle make (i.e.Chevy)   ",
      "type": "string"
    },
    "makeName": {
      "description": "ull name of the make (i.e. Chevrolet)",
      "type": "string"
    },
    "modelCode": {
      "description": "Model code the marketing year defined by the OEM which the vehicle was produced ",
      "type": "string"
    },
    "modelDescription": {
      "description": "Model description the marketing year defined by the OEM which the vehicle was produced. ",
      "type": "string"
    },
    "modelYear": {
      "type": "string"
    },
    "bodyStyleCode": {
      "description": "equivalent code value of the bodyStyle description. CP=Coupe. POLK will send code value",
      "type": "string"
    },
    "bodyStyleDescription": {
      "description": "description value of body style/ type of the vehicle ex: suv, bus, convertible, coupe and etc   ",
      "type": "string"
    },
    "securitySystemCode": {
      "description": "equivalent code value of the security system description    ",
      "type": "string"
    },
    "segmentationCode": {
      "description": "equivalent code value of the standard segmentation description  ",
      "type": "string"
    },
    "seriesCode": {
      "type": "string"
    },
    "seriesDescription": {
      "type": "string"
    },
    "transmissionCode": {
      "type": "string"
    },
    "grossWeight": {
      "description": "Numeric valuf anything equivalent to code or description    ",
      "type": "number",
      "format": "double"
    },
    "grossWeightCode": {
      "description": "equivalent code value of the Gross Vehicle Weight description   ",
      "type": "string"
    },
    "grossWeightDescription": {
      "description": "description of manufacturers assigned Gross Vehicle Weight  ",
      "type": "string"
    },
    "tonnageRatingCode": {
      "type": "string"
    },
    "wheelBaseLongestDistance": {
      "description": "longest distance between the front and rear axles of a vehicle  ",
      "type": "number",
      "format": "double"
    },
    "price": {
      "description": "This construct will be defined as unbound so that we can handle/map different prices like manufacturer base price and/ or price with optional elements and etc. ",
      "type": "array",
      "items": {
        "$ref": "Price"
      }
    },
    "countryofOrigin": {
      "description": "This is the country where the plant is located. Example values are USA, Canada and Japan.   ",
      "type": "string"
    },
    "vehicleAssesment": {
      "description": "New complex construct VehicleAssesment defined as unbound to map one or more assesment attributes from NADA SERIES1,2, 3 and etc vehicle    ",
      "type": "array",
      "items": {
        "$ref": "VehicleAssesment"
      }
    }
  }
}
*/

public class Vehicle {

	@Size(max=1)
	private Integer vehicleNumber;

	@Size(max=1)
	private String vin;

	@Size(max=1)
	private String garageStateCode;

	@Size(max=1)
	private String engineCylinderCount;

	@Size(max=1)
	private String typeCode;

	@Size(max=1)
	private String makeCode;

	@Size(max=1)
	private String makeName;

	@Size(max=1)
	private String modelCode;

	@Size(max=1)
	private String modelDescription;

	@Size(max=1)
	private String modelYear;

	@Size(max=1)
	private String bodyStyleCode;

	@Size(max=1)
	private String bodyStyleDescription;

	@Size(max=1)
	private String securitySystemCode;

	@Size(max=1)
	private String segmentationCode;

	@Size(max=1)
	private String seriesCode;

	@Size(max=1)
	private String seriesDescription;

	@Size(max=1)
	private String transmissionCode;

	@Size(max=1)
	private Double grossWeight;

	@Size(max=1)
	private String grossWeightCode;

	@Size(max=1)
	private String grossWeightDescription;

	@Size(max=1)
	private String tonnageRatingCode;

	@Size(max=1)
	private Double wheelBaseLongestDistance;

	@Size(max=1)
	private List<com.cna.services.Policy.cnaPolicy.PolicyDSM.AssessVehicle.v1.Price> price;

	@Size(max=1)
	private String countryofOrigin;

	@Size(max=1)
	private List<com.cna.services.Policy.cnaPolicy.PolicyDSM.AssessVehicle.v1.VehicleAssesment> vehicleAssesment;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    
	    vin = null;
	    garageStateCode = null;
	    engineCylinderCount = null;
	    typeCode = null;
	    makeCode = null;
	    makeName = null;
	    modelCode = null;
	    modelDescription = null;
	    modelYear = null;
	    bodyStyleCode = null;
	    bodyStyleDescription = null;
	    securitySystemCode = null;
	    segmentationCode = null;
	    seriesCode = null;
	    seriesDescription = null;
	    transmissionCode = null;
	    
	    grossWeightCode = null;
	    grossWeightDescription = null;
	    tonnageRatingCode = null;
	    
	    price = new ArrayList<com.cna.services.Policy.cnaPolicy.PolicyDSM.AssessVehicle.v1.Price>();
	    countryofOrigin = null;
	    vehicleAssesment = new ArrayList<com.cna.services.Policy.cnaPolicy.PolicyDSM.AssessVehicle.v1.VehicleAssesment>();
	}
	public Integer getVehicleNumber() {
		return vehicleNumber;
	}
	
	public void setVehicleNumber(Integer vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getVin() {
		return vin;
	}
	
	public void setVin(String vin) {
		this.vin = vin;
	}
	public String getGarageStateCode() {
		return garageStateCode;
	}
	
	public void setGarageStateCode(String garageStateCode) {
		this.garageStateCode = garageStateCode;
	}
	public String getEngineCylinderCount() {
		return engineCylinderCount;
	}
	
	public void setEngineCylinderCount(String engineCylinderCount) {
		this.engineCylinderCount = engineCylinderCount;
	}
	public String getTypeCode() {
		return typeCode;
	}
	
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}
	public String getMakeCode() {
		return makeCode;
	}
	
	public void setMakeCode(String makeCode) {
		this.makeCode = makeCode;
	}
	public String getMakeName() {
		return makeName;
	}
	
	public void setMakeName(String makeName) {
		this.makeName = makeName;
	}
	public String getModelCode() {
		return modelCode;
	}
	
	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}
	public String getModelDescription() {
		return modelDescription;
	}
	
	public void setModelDescription(String modelDescription) {
		this.modelDescription = modelDescription;
	}
	public String getModelYear() {
		return modelYear;
	}
	
	public void setModelYear(String modelYear) {
		this.modelYear = modelYear;
	}
	public String getBodyStyleCode() {
		return bodyStyleCode;
	}
	
	public void setBodyStyleCode(String bodyStyleCode) {
		this.bodyStyleCode = bodyStyleCode;
	}
	public String getBodyStyleDescription() {
		return bodyStyleDescription;
	}
	
	public void setBodyStyleDescription(String bodyStyleDescription) {
		this.bodyStyleDescription = bodyStyleDescription;
	}
	public String getSecuritySystemCode() {
		return securitySystemCode;
	}
	
	public void setSecuritySystemCode(String securitySystemCode) {
		this.securitySystemCode = securitySystemCode;
	}
	public String getSegmentationCode() {
		return segmentationCode;
	}
	
	public void setSegmentationCode(String segmentationCode) {
		this.segmentationCode = segmentationCode;
	}
	public String getSeriesCode() {
		return seriesCode;
	}
	
	public void setSeriesCode(String seriesCode) {
		this.seriesCode = seriesCode;
	}
	public String getSeriesDescription() {
		return seriesDescription;
	}
	
	public void setSeriesDescription(String seriesDescription) {
		this.seriesDescription = seriesDescription;
	}
	public String getTransmissionCode() {
		return transmissionCode;
	}
	
	public void setTransmissionCode(String transmissionCode) {
		this.transmissionCode = transmissionCode;
	}
	public Double getGrossWeight() {
		return grossWeight;
	}
	
	public void setGrossWeight(Double grossWeight) {
		this.grossWeight = grossWeight;
	}
	public String getGrossWeightCode() {
		return grossWeightCode;
	}
	
	public void setGrossWeightCode(String grossWeightCode) {
		this.grossWeightCode = grossWeightCode;
	}
	public String getGrossWeightDescription() {
		return grossWeightDescription;
	}
	
	public void setGrossWeightDescription(String grossWeightDescription) {
		this.grossWeightDescription = grossWeightDescription;
	}
	public String getTonnageRatingCode() {
		return tonnageRatingCode;
	}
	
	public void setTonnageRatingCode(String tonnageRatingCode) {
		this.tonnageRatingCode = tonnageRatingCode;
	}
	public Double getWheelBaseLongestDistance() {
		return wheelBaseLongestDistance;
	}
	
	public void setWheelBaseLongestDistance(Double wheelBaseLongestDistance) {
		this.wheelBaseLongestDistance = wheelBaseLongestDistance;
	}
	public List<com.cna.services.Policy.cnaPolicy.PolicyDSM.AssessVehicle.v1.Price> getPrice() {
		return price;
	}
	
	public void setPrice(List<com.cna.services.Policy.cnaPolicy.PolicyDSM.AssessVehicle.v1.Price> price) {
		this.price = price;
	}
	public String getCountryofOrigin() {
		return countryofOrigin;
	}
	
	public void setCountryofOrigin(String countryofOrigin) {
		this.countryofOrigin = countryofOrigin;
	}
	public List<com.cna.services.Policy.cnaPolicy.PolicyDSM.AssessVehicle.v1.VehicleAssesment> getVehicleAssesment() {
		return vehicleAssesment;
	}
	
	public void setVehicleAssesment(List<com.cna.services.Policy.cnaPolicy.PolicyDSM.AssessVehicle.v1.VehicleAssesment> vehicleAssesment) {
		this.vehicleAssesment = vehicleAssesment;
	}
}