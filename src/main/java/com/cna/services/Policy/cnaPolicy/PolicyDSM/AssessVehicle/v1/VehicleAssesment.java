package com.cna.services.Policy.cnaPolicy.PolicyDSM.AssessVehicle.v1;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for VehicleAssesment:
{
  "type": "object",
  "properties": {
    "id": {
      "type": "string"
    },
    "bodyStyleCode": {
      "type": "string"
    },
    "price": {
      "type": "array",
      "items": {
        "$ref": "Price"
      }
    }
  }
}
*/

public class VehicleAssesment {

	@Size(max=1)
	private String id;

	@Size(max=1)
	private String bodyStyleCode;

	@Size(max=1)
	private List<com.cna.services.Policy.cnaPolicy.PolicyDSM.AssessVehicle.v1.Price> price;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    id = null;
	    bodyStyleCode = null;
	    price = new ArrayList<com.cna.services.Policy.cnaPolicy.PolicyDSM.AssessVehicle.v1.Price>();
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getBodyStyleCode() {
		return bodyStyleCode;
	}
	
	public void setBodyStyleCode(String bodyStyleCode) {
		this.bodyStyleCode = bodyStyleCode;
	}
	public List<com.cna.services.Policy.cnaPolicy.PolicyDSM.AssessVehicle.v1.Price> getPrice() {
		return price;
	}
	
	public void setPrice(List<com.cna.services.Policy.cnaPolicy.PolicyDSM.AssessVehicle.v1.Price> price) {
		this.price = price;
	}
}