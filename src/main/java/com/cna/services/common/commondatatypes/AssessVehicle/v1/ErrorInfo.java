package com.cna.services.common.commondatatypes.AssessVehicle.v1;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for ErrorInfo:
{
  "type": "object",
  "properties": {
    "errorMessageType": {
      "type": "string"
    },
    "errorCode": {
      "type": "string"
    },
    "errorMessageText": {
      "type": "string"
    },
    "errorState": {
      "type": "string"
    }
  }
}
*/

public class ErrorInfo {

	@Size(max=1)
	private String errorMessageType;

	@Size(max=1)
	private String errorCode;

	@Size(max=1)
	private String errorMessageText;

	@Size(max=1)
	private String errorState;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    errorMessageType = null;
	    errorCode = null;
	    errorMessageText = null;
	    errorState = null;
	}
	public String getErrorMessageType() {
		return errorMessageType;
	}
	
	public void setErrorMessageType(String errorMessageType) {
		this.errorMessageType = errorMessageType;
	}
	public String getErrorCode() {
		return errorCode;
	}
	
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessageText() {
		return errorMessageText;
	}
	
	public void setErrorMessageText(String errorMessageText) {
		this.errorMessageText = errorMessageText;
	}
	public String getErrorState() {
		return errorState;
	}
	
	public void setErrorState(String errorState) {
		this.errorState = errorState;
	}
}