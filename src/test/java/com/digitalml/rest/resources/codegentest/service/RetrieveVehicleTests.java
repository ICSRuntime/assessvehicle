package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.AssessVehicle.AssessVehicleServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.AssessVehicleService.RetrieveVehicleInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.AssessVehicleService.RetrieveVehicleReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveVehicleTests {

	@Test
	public void testOperationRetrieveVehicleBasicMapping()  {
		AssessVehicleServiceDefaultImpl serviceDefaultImpl = new AssessVehicleServiceDefaultImpl();
		RetrieveVehicleInputParametersDTO inputs = new RetrieveVehicleInputParametersDTO();
		inputs.setHeaderMetadata(new com.cna.services.framework.cnaHeader.v2.HeaderMetadata());
		inputs.setTo(new com.cna.services.framework.cnaHeader.v2.To());
		inputs.setConsumerInfo(new com.cna.services.framework.cnaHeader.v2.ConsumerInfo());
		inputs.setRetrieveVehicle(new com.cna.services.Underwriting.AssessVehicle.contract.v1.RetrieveVehicle());
		RetrieveVehicleReturnDTO returnValue = serviceDefaultImpl.retrieveVehicle(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getHeaderMetadata());
		assertNotNull(returnValue.getTo());
		assertNotNull(returnValue.getConsumerInfo());
		assertNotNull(returnValue.getRetrieveVehicleResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}