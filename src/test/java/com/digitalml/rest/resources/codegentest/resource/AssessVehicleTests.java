package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class AssessVehicleTests {

	@Test
	public void testResourceInitialisation() {
		AssessVehicleResource resource = new AssessVehicleResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationRetrieveVehicleNoSecurity() {
		AssessVehicleResource resource = new AssessVehicleResource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveVehicle(new com.cna.services.framework.cnaHeader.v2.HeaderMetadata(), new com.cna.services.framework.cnaHeader.v2.To(), new com.cna.services.framework.cnaHeader.v2.ConsumerInfo(), new com.cna.services.Underwriting.AssessVehicle.contract.v1.RetrieveVehicle());
		Assert.assertEquals(403, response.getStatus());
	}


	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}